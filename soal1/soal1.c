#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <stdbool.h>
#include <wait.h>
#include <time.h>
#include <dirent.h> 
#include <limits.h>


void create_dir(){
    int status;

    if (fork() == 0){
        char *argv[] = {"mkdir", "/home/wahid/sisop/modul2/gacha_gacha", NULL};
        execv("/bin/mkdir", argv);
    }
}

void download_file(){
    char *files[] = {
        "https://drive.google.com/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download", "characters.zip",
        "https://drive.google.com/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download", "weapons.zip"
    };

    int status;
    pid_t child = fork();

    if(child < 0) exit(EXIT_FAILURE);

    for (int i = 0; i < 4; i += 2){
        if(child == 0){
            char *argv[] = {"wget", "-q", "--no-check-certificate", files[i], "-O", files[i + 1], NULL};
            execv("/bin/wget", argv);
        }
        while((wait(&status)) > 0);
        child = fork();
    }
}

void extract_files() {
    char *files[] = {"characters.zip", "weapons.zip"};

    int status;
    pid_t child = fork();

    for (int i = 0; i < 2; i++){
        if(child == 0){
            char *argv[] = {"unzip", files[i], NULL};
            execv("/bin/unzip", argv);
        }
        while((wait(&status)) > 0);
        child = fork();
    }
}

void move_files(){
    pid_t child = fork();
    int status;

    char cwd[PATH_MAX];
    getcwd(cwd, PATH_MAX);

    DIR *dir;
    struct dirent *dp;
    char current[PATH_MAX];
    char dest[PATH_MAX];

    char *files[] = {"characters", "weapons"};

    for (int i = 0; i < 2; i++){
        strcpy(current, cwd);
        strcpy(dest, "/home/wahid/sisop/modul2/gacha_gacha/"); 
        dir = opendir(current);

        while((dp = readdir(dir)) != NULL){
            if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strstr(dp->d_name, "soal") == 0){
                if(child == 0){
                    char file[PATH_MAX];
                    strcpy(file, current);
                    strcat(file, "/");
                    strcat(file, dp->d_name);
                    
                    char *argv[] = {"mv", file, dest, NULL};
                    execv("/bin/mv", argv);
                }
                while((wait(&status)) > 0);
                child = fork();
            }
        }
        closedir(dir);
    }
}

void delete_zip_dir(){
    DIR *dir;
    struct dirent *dp;
    char path[PATH_MAX];

    pid_t child = fork();
    int status;

    strcpy(path, "/home/wahid/sisop/modul2/gacha_gacha/");
    dir = opendir(path);

    while((dp = readdir(dir)) != NULL){
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 
            && strcmp(dp->d_name, "weapons") != 0 && strcmp(dp->d_name, "characters") != 0){

            strcpy(path, "/home/wahid/sisop/modul2/gacha_gacha/");
            strcat(path, dp->d_name);

            if(child == 0){
                char *argv[] = {"rm", "-rf", path, NULL};
                execv("/bin/rm", argv);
            }
            while((wait(&status)) > 0);
            child = fork();
        }
    }
    closedir(dir);
}

int main(){
    pid_t pid, sid;

    pid = fork();

    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    char cwd[PATH_MAX];
    if ((chdir(getcwd(cwd, PATH_MAX))) < 0) {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    int flag = 0;
    while (flag == 0) {
        // time_t t = time(NULL);
        // struct tm tm = *localtime(&t);
        // printf("%d %d %d %d %d\n", tm.tm_mon, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);

        // create_dir();
        // download_file();
        // extract_files();
        // move_files();
        // delete_zip_dir();

        flag = 1;
    }
}